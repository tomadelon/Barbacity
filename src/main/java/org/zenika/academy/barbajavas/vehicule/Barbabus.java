package org.zenika.academy.barbajavas.vehicule;

public class Barbabus extends Vehicule{

    int nbRoues;
    String carburant;
    String noise;

    public Barbabus(int nbRoues, String carburant, String immatriculation, boolean isAvailable, String noise) {
        super(immatriculation, isAvailable);
        this.nbRoues = 4;
        this.carburant = "Barbalight";
        this.noise = "BARBAPOUET !";
    }
}
