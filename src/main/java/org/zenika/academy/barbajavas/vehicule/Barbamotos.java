package org.zenika.academy.barbajavas.vehicule;

public class Barbamotos extends Vehicule{

    int nbRoues;
    String carburant;
    String noise;

    public Barbamotos(int nbRoues, String carburant, String immatriculation, boolean isAvailable, String noise) {
        super(immatriculation, isAvailable);
        this.nbRoues = 2;
        this.carburant = "Barbafuel";
        this.noise = "BARBADAM !!";
    }
}
