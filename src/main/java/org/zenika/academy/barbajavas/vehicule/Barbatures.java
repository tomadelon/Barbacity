package org.zenika.academy.barbajavas.vehicule;

public class Barbatures extends Vehicule{

    int nbRoues;
    String carburant;
    String noise;

    public Barbatures(int nbRoues, String carburant, String immatriculation, boolean isAvailable, String noise) {
        super(immatriculation, isAvailable);
        this.nbRoues = 3;
        this.carburant = "Barbasiel";
        this.noise = "BRABAVROUM !!!";
    }
}
